extends Node

# Score limit to prevent breaking the UI
var MAX_SCORE = 99999999
# The save game path
var SAVE_GAME_PATH = "user://game_save.save"

var score = 0

func _ready():
	randomize()
	$BackgroundMusic.play()

func _on_Bird_start_flight():
	# Player has initiated control, prepare to start generating tiles
	print ("masuk flight mode")
	$StartTileSpawningTimeout.start()
	# Show the score
	$HUD.show_mode($HUD.State.PLAYING)

func _on_StartTileSpawningTimeout_timeout():
	# Enough time has passed to acclimatize the user, start generating objects
	print ("tile spawning")
	$TileSpawner.start_generation()

func _on_TileSpawner_score_point():
	score += 1
	$HUD.set_score_label(score)

func _on_Bird_death():
	# Stop further tile generation
	$TileSpawner.stop_generation()
	# Stop the tiles from moving further
	for tile in $TileSpawner/GeneratedTiles.get_children():
		tile.set_active(false)
	# Stop the ground and background from scrolling
	$Lava.set_active(false)
	$Background.set_active(false)
	# Check for a previous best value, ensuring it is within the allowed range to not break the UI
	var previous_best = load_save()
	previous_best = clamp(previous_best, 0, MAX_SCORE)
	# If the current score is better, write in the new score
	if previous_best <= score:
		write_save(score)
	# Update the previous best in the HUD
	$HUD.set_previous_best(previous_best)
	# Show the menu
	$HUD.show_mode($HUD.State.MENU)

func _on_HUD_restart():
	# Wipe all the tiles currently on the screen
	$TileSpawner.wipe_tiles()
	# Get the ground and background scrolling again
	$Lava.set_active(true)
	$Background.set_active(true)
	# Reset the score
	score = 0
	$HUD.set_score_label(score)
	# Reset the player
	$Dragon.set_player_state($Dragon.State.AUTO_PILOT)
	# Set the UI back to standby mode
	$HUD.show_mode($HUD.State.SETUP)

func load_save():
	var saved_game = File.new()
	# If there is no save file, just return a previous best of 0
	if not saved_game.file_exists(SAVE_GAME_PATH):
		return 0
	# Otherwise load up the save file
	saved_game.open(SAVE_GAME_PATH, File.READ)
	# Load up the value
	var save = parse_json(saved_game.get_line())
	# Close the file since we're done with it
	saved_game.close()
	# Return the previous best value
	return save["best"]

func write_save(new_best):
	# Prepare the save file for writing
	var saved_game = File.new()
	saved_game.open(SAVE_GAME_PATH, File.WRITE)
	# Create the dictionary for writing
	var save = to_json({"best": new_best})
	# Write the save
	saved_game.store_line(save)
	# Close the file since we're done with it
	saved_game.close()
